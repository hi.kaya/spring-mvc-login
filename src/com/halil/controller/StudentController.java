package com.halil.controller;

import java.util.List;

import com.halil.springdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.halil.springdemo.dao.StudentDAO;
import com.halil.springdemo.entity.Student;


@Controller
@RequestMapping(value = "/student",method = RequestMethod.GET)
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	

	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public String listStudents(Model theModel) {
		
	
		List<Student> theStudent = studentService.getStudents();
	
		theModel.addAttribute("students",theStudent);
		
		return "list-student";
	}

	@GetMapping("/showStudentForm")
	public String showStudentForm(Model theModel){

		Student theStudent = new Student();
		theModel.addAttribute("student",theStudent);

		return "student-form";
	}

	@PostMapping("/saveStudent")
	public String saveStudent(@ModelAttribute("student") Student theStudent){

 		studentService.saveStudent(theStudent);

		return "redirect:/student/list";
	}

	@GetMapping("/showUpdateForm")
	public String showUpdateForm(@RequestParam("studentID") int theId,Model theModel  ){

		Student theStudent = studentService.getStudent(theId);

		theModel.addAttribute(theStudent);

		return "student-form";
	}

	@GetMapping("/deleteStudent")
	public String deleteStudent(@RequestParam("studentID") int theId){
		studentService.deleteStudent(theId);

		//öğrenci sayfasına tekrar dönmek için
		return "redirect:/student/list";

	}


}
