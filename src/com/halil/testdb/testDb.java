package com.halil.testdb;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/testDb")
public class testDb extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request ,HttpServletResponse response ) throws ServletException,IOException{
		String user="halil";
		String pass="halil";
		String jdbcUrl="jdbc:mysql://localhost:3306/ogrenci_schema?useSSL=false";
		String driver="com.mysql.jdbc.Driver";
		
		try	{
			PrintWriter out=response.getWriter();
			out.println("Veri tabani baglanti : " +jdbcUrl);
			Class.forName(driver);
			Connection myConn = DriverManager.getConnection(jdbcUrl,user,pass);
			out.println("Basarili!");
			myConn.close(); 
		}catch(Exception e) {
			e.printStackTrace();
			throw new ServletException();
		}
		
	}
	
}
