package com.halil.springdemo.dao;

import java.util.List;

import com.halil.springdemo.entity.Student;

public interface StudentDAO {

    Student getStudent(int theId);

    public List<Student> getStudents();

	void saveStudent(Student theStudent);

    void deleteStudent(int theId);
}
