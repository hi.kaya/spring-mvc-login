package com.halil.springdemo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.halil.springdemo.entity.Student;
//veri tabanından data çekeceğimiz zaman kullanıyoruz

@Repository
public class StudentDAOImp implements StudentDAO {
 
	
	//hibernateden session nesnesini aldık.hibernate sessionu get edeceğimiz factorysi lazım diye Sessşon factory kullandık

	@Autowired
	private SessionFactory sessionFactory;
	
	
	//transactional data alıp verme için

	@Override
	public Student getStudent(int theId) {

		Session currentSession = sessionFactory.getCurrentSession();

		Student theStudent=currentSession.get(Student.class,theId);

		return theStudent;
	}

	@Override
	public List<Student> getStudents() {
		
		//
		Session currentSession = sessionFactory.getCurrentSession();
		
		//hibernate e özel bir şey.Studenttan bütün studentları al--SELECT * FROM STudents le aynı anlama geliyors
		
		Query<Student> theQuery = currentSession.createQuery("from Student", Student.class);
		
		//üstte query sonunda dönen listeyi alıyorum
		
		List<Student> students = theQuery.getResultList();
		
		//aldığım studentsleri döndürdüm
		
		return students;
	}

	@Override
	public void saveStudent(Student theStudent) {
		Session currentSession = sessionFactory.getCurrentSession();
		currentSession.saveOrUpdate(theStudent);
	}

	@Override
	public void deleteStudent(int theId) {
		Session currentSession = sessionFactory.getCurrentSession();

		Query theQuery = currentSession.createQuery("delete from Student where id=:studentId");

		theQuery.setParameter("studentId",theId);

		theQuery.executeUpdate();

	}

}
