package com.halil.springdemo.service;

import com.halil.springdemo.entity.Student;

import java.util.List;

public interface StudentService {

    public Student getStudent(int theId) ;

    public List<Student> getStudents();

    public void saveStudent(Student theStudent);

    public void deleteStudent(int theId);
}
