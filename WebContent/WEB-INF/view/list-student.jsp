<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
		  integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>
<div class="container">

	<div class="jumbotron">
		<h1 class="display-4">Ogrenci Sistemine Hosgeldiniz </h1>
		<hr class="my-4">
		<!--
		<a class="btn btn-success" onclick="window.location.href='showStudentForm'; return false;" type="button" href="#" role="button">Ogrenci Ekle</a>
		-->
		<button  onclick="window.location.href='showStudentForm'; return false;" type="button" type="submit" Class="btn btn-primary"> Ogrenci Ekle</button>

	</div>
	<div>
		<table class="table table-striped">
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>E-mail</th>
				<th>Update/Delete</th>
			</tr>
			<!-- students controllerdakiyle aynı olmalı ordan buraya yolluyoruz   -->

			<c:forEach var="tempStudent" items="${students}">
				<tr>
					<td>${tempStudent.firstName}</td>
					<td>${tempStudent.lastName}</td>
					<td>${tempStudent.email}</td>
					<td><a class="btn btn-warning" href="${pageContext.request.contextPath}/student/showUpdateForm?studentID=${tempStudent.id}">Update</a>
						<a class="btn btn-danger"  href="${pageContext.request.contextPath}/student/deleteStudent?studentID=${tempStudent.id}" onclick="if(!(confirm('Are you sure?'))) return false;">Delete</a>
					</td>

				</tr>
			</c:forEach>
		</table>
	</div>
</div>


</body>
</html>