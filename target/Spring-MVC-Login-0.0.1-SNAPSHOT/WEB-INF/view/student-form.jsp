<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Insert title here</title>
</head>
<body>

<div class="container">
    <div class="jumbotron">
        <h1>Add New Student</h1>
    </div>
    <div>
        <form:form Class="form-horizantal" modelAttribute="student" action="saveStudent" method="post">
            <form:hidden path="id"></form:hidden>

            <div class="form-group">
                <label for="name" class="col-sm-10 control-label">Ogrenci Adi</label>
                <div>
                    <!-- pathler sayesinde tek tek requestin içindeki first name demeden direk içine atıyor spring.  -->
                    <form:input type="text" Class="form-control" id="name" placeholder="Name" path="firstName"></form:input>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-10 control-label">Ogrenci Soyadi</label>
                <div>
                    <form:input type="text" Class="form-control" id="lastname" placeholder="Lastname" path="lastName"></form:input>
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-10 control-label">Email</label>
                <div>
                    <form:input type="text" Class="form-control" id="email" placeholder="Email" path="email"></form:input>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-10">
                    <button type="submit" Class="btn btn-primary">Ekle</button>
                </div>
            </div>




        </form:form>
    </div>
    <br>
    <p>
        <a href="${pageContext.request.contextPath}/student/list">Ogrenci Listesine Geri Git</a>
    </p>



</div>



</body>
</html>